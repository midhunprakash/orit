<?php
class Database
{
  //DB connection
  public function __construct()
  {
      $servername = "localhost";
      $username = "root";
      $password = "";
      $db = "orit";

      // Create connection
      $conn = mysqli_connect($servername, $username, $password, $db);
      $this->dbh=$conn;
      // Check connection
      if (mysqli_connect_errno()){
          die("Connection failed: " . mysqli_connect_error());
      }
  }

  public function insertWord($word)
  {
    $ret=mysqli_query($this->dbh,"insert into words(word) values('$word')");
    return $ret;
  }

  public function getWordCount()
  {
    $sql="SELECT DISTINCT(word) FROM words";

    if ($result=mysqli_query($this->dbh,$sql))
    {
    // Return the number of rows in result set
    $rowcount=mysqli_num_rows($result);
    printf("Distinct unique words: %d. \n",$rowcount);

    $result_array = array();

    while($row = mysqli_fetch_assoc($result)){
      $result_array[] = $row;
    }

    mysqli_free_result($result);
    }

    return $result_array;

  }

  public function getWatchList()
  {
    $query = "SELECT word FROM watchlist";
    $result = mysqli_query($this->dbh, $query);
    // $row = mysqli_fetch_assoc($result);

    $watch_list_array = array();

    while($row = mysqli_fetch_assoc($result)){
      $watch_list_array[] = $row;
    }

    return $watch_list_array;
  }
}
